--- 
title: "The influence of weight trimming on model estimation in complex survey data and significance test"
author:
  - Wenjie Wang^[<wenjie.2.wang@uconn.edu>; Ph.D. student at
    Department of Statistics, University of Connecticut.]
date: "`r Sys.Date()`"
site: bookdown::bookdown_site
output: bookdown::gitbook
documentclass: article
lof: no
lot: no
toc: no
bibliography: [book.bib, packages.bib, BibTex.bib]
biblio-style: apalike
link-citations: yes

abstract: 
    In sample surveys, the sample units are typically chosen using a complex design. This may lead to a selection effect but a unit's inclusion probability is unequal. Researchers apply sampling weights to take account of unequal sample selection probabilities and to frame coverage errors and non-responses. If we do not weight when appropriate, we risk having biased estimates. Alternatively, when we unnecessarily apply weights, we can create an inefficient estimator without reducing bias.
    
keywords: Template, R Markdown, bookdown, Data Lab

github-repo: rstudio/bookdown-demo
description: "This is a minimal example of using the bookdown package to write a book. The output format for this example is bookdown::gitbook."
---

# Introduction

In sample surveys, the sample units are typically chosen using a complex design. This may lead to a selection effect but a unit's inclusion probability is unequal[@chenApproachesImprovingSurveyWeighted2017]. Researchers apply sampling weights to take account of unequal sample selection probabilities and to frame coverage errors and non-responses. If we do not weight when appropriate, we risk having biased estimates. Alternatively, when we unnecessarily apply weights, we can create an inefficient estimator without reducing bias[@bollenAreSurveyWeights2016].



# weight trimming 

One of the advantages of making use of complex sampling data is the improved estimation of quantities through the use of sampling weights. Unfortunately, these sampling weights may increase the sampling variance of the estimators due to large variation in their values and consequently result in lower inference precision [@unstatisticsdivisionDesigningHouseholdSurvey2008]. Such variation could be attributed to the specific sampling procedure used, errors in the sampling frame, non-response adjustments or various other sources [@potterSTUDYPROCEDURESIDENTIFY1990]. The trimming or truncation of weights identified to be extreme could assist in the reduction of the large weight variation which in turn would improve the precision of the estimation [@chowdhuryWeightTrimmingNational2007].

A number of trimming or truncation techniques have been proposed in the literature, all sharing the same goal, that is, to modify the survey weights so that the resulting estimators have a lower mean square error than that of the usual estimators (e.g., the Horvitz–Thompson estimator). Weight trimming consists of modifying the weight of units that are identified as “influential”. The concept of influential unit is not always clearly defined in practice [@chenApproachesImprovingSurveyWeighted2017]. In some cases, a unit is identified as influential if it exhibits a large weight; In other cases, a unit showing a large weighted value (i.e., $w_iy_i$ ) is labeled influential; More recently, @moreno-rebolloMiscellaneaInfluenceDiagnostic1999a ; @moreno-rebolloInfluenceDiagnosticSurvey2002 and @beaumontUnifiedApproachRobust2013 used the concept of conditional bias of a unit as a measure of influence in the context of finite population sampling. 

# Method


# The Simulated Population

This section will describe how a population will be simulated from which a complex sample will be selected.












































